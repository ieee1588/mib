Rem Attribute VBA_ModuleType=VBAModule
Option VBASupport 1
' Copyright Hubert Kirrmann 2013-2021
' Last update 2021-03-23 HK
' This macro generates an SNMP MIB (Management Information Base) that can
' be processed by an SNMPv3 conformant network management tool.
' This macro has been developped for the IEC SC65C WG15 and for the
' P1588 Management Task Force since the year 2013
' to generate the MIB of the IEEE 1588 precision time protocol profile IEC 62439-3.
' This macro creates the MIB file out of the Excel sheets.
' it could generate a file #2 that can used in a Word document (but this is not complete).
' The only influence of this macro on the data is the generation of the OID in Sheet Objects
' from level information - to avoid entering duplicate information.
' Recent changes:
' column labelling can be different in each worksheets, only the name matters.

Option Explicit

' worksheets
Public SheetReadMe As Excel.Worksheet     ' worksheet with conventions and file location
Public SheetSettings As Excel.Worksheet   ' worksheet with current list of objects
Public SheetObjects As Excel.Worksheet    ' worksheet with current list of objects
Public SheetTextual As Excel.Worksheet    ' worksheet with current list of type definitions
Public SheetEvents As Excel.Worksheet     ' worksheet with current list of notifications
Public SheetGroups As Excel.Worksheet     ' worksheet with current list of conformance groups

' number of the column in the Textual sheet
Public TxtColControl As Integer     ' column number in Textual worksheet for the control
Public TxtColName As Integer        ' column number in Textual worksheet for the name
Public TxtColReference As Integer   ' column number in Textual worksheet for the reference
Public TxtColSyntax As Integer      ' column number in Textual worksheet for the syntax
Public TxtColDescription As Integer ' column number in Textual worksheet for the description
Public TxtColUnits As Integer       ' column number in Textual worksheet for the units
Public TxtColHint As Integer        ' column number in Textual worksheet for the hint

' number of the column in the Objects sheet
Public ObjColControl As Integer     ' column number in Objects worksheet for the control
Public ObjColOID As Integer         ' column number in Objects worksheet for the OID
Public ObjColTree As Integer        ' column number in Objects worksheet for the level
Public ObjColName As Integer        ' column number in Objects worksheet for the name
Public ObjColReference As Integer   ' column number in Objects worksheet for the reference
Public ObjColSyntax As Integer      ' column number in Objects worksheet for the syntax
Public ObjColUnits As Integer       ' column number in Objects worksheet for the units
Public ObjColDefVal As Integer      ' column number in Objects worksheet for the default value
Public ObjColStatus As Integer      ' column number in Objects worksheet for the status
Public ObjColDescription As Integer ' column number in Objects worksheet for the description
Public ObjColAccess As Integer      ' column number in Objects worksheet for the access
Public ObjColHint As Integer        ' column number in Objects worksheet for the display hint
Public ObjColGroup As Integer       ' column number in Objects worksheet for the conformance group

' number of the column in the Events sheet
Public EvtColControl As Integer     ' column number in Events worksheet for the control
Public EvtColName As Integer        ' column number in Events worksheet for the name
Public EvtColTree As Integer        ' column number in Events worksheet for the tree
Public EvtColTrigger As Integer     ' column number in Events worksheet for the trigger
Public EvtColStatus As Integer      ' column number in Events worksheet for the status
Public EvtColDescription As Integer ' column number in Events worksheet for the description
Public EvtColGroup As Integer       ' column number in Events worksheet for the group

' number of the column in the Groups sheet
Public GrpColControl As Integer     ' column number in Groups worksheet for the control
Public GrpColName As Integer        ' column number in Groups worksheet for the name
Public GrpColOption As Integer      ' column number in Groups worksheet for the option
Public GrpColStatus As Integer      ' column number in Groups worksheet for the status
Public GrpColDescription As Integer ' column number in Groups worksheet for the description
Public GrpColConformance As Integer ' column number in Groups worksheet for the conformance

Public SettingsCursor As Integer    ' Cursor indicating the row in the settings table (static for GetSetting)
Public TabChar As String            ' just to enter a tab, anywhere

Public MibStackObject(20) As String ' the name of the OID levels
Public MibStackCnt(20)  As Integer  ' the number of objects of each OID level
Public MibStackLevel   As Integer   ' index of MibStackObject and MibStackCnt - current level within the OID

Public ObjectLevel  As Integer      ' the OID level

Public GrpGroup(50) As String       ' table of conformance groups names
Public GrpEntries As Integer
Public mibDefinition As String

Public wordArraySm() as Variant     ' array with words using master/slave
Public wordArrayTr() as Variant     ' array with words using time transmitter/time receiver

Public wordingVersion as String     ' String indicating whihc wording is used MS (master-slave) or TR
                                    ' TimeTransmitter-TimeReceiver


Public Sub MakeMibMS()
    wordingVersion="MS"
    call MakeMib()
End Sub

Public Sub MakeMibTr()
    wordingVersion="TT"
    call MakeMib()
End Sub

Public Sub MakeMib()
' See introduction on top of this file.

Dim mibStr As String
Dim syntaxStr As String

Set SheetReadMe = ActiveWorkbook.Worksheets("ReadMe")      ' for the files
Set SheetSettings = ActiveWorkbook.Worksheets("Settings")  ' for the settings
Set SheetTextual = ActiveWorkbook.Worksheets("Textual")    ' for the textual conventions
Set SheetObjects = ActiveWorkbook.Worksheets("Objects")    ' these are the objects
Set SheetEvents = ActiveWorkbook.Worksheets("Events")      ' for the settings
Set SheetGroups = ActiveWorkbook.Worksheets("Groups")      ' for the conformance groups

SettingsCursor = 1 'initial value

TabChar = "    " 'Chr(9)

'=========== Header
Dim mibFileLocation As String
Dim mibFilePrefix As String
Dim mibFileName As String
Dim mibImports As String
Dim mibIdentity As String
Dim mibDescription As String
Dim mibDummy As String
Dim mibRoot As String

call BuildArrayForSmReplacement()

mibFileLocation = SheetReadMe.Cells(37, 2)
' get the file name from definitions
 If (GetSetting("DEFINITIONS", mibDefinition, mibDummy)) Then
    mibDefinition = mibDefinition + "-" + wordingVersion + "-MIB"
    ' use it for the file name
    mibFilePrefix = mibDefinition
    mibFileName = mibFileLocation + mibFilePrefix + ".mib" ' file name without path
    Open mibFileName For Output As #1
    ' Open mibFilePrefix & ".txt" For Output As #2 ' === Placeholder for TXT file
    ' write the name of the module
    mibStr = mibDefinition + " DEFINITIONS ::= BEGIN"
    Print #1, mibStr
    ' Print #2, "Type"
End If
' read the import list
mibStr = "IMPORTS"
Print #1, mibStr
    
While (GetSetting("IMPORTS", mibImports, mibDummy))
   Print #1, TabChar + mibImports
Wend

' read the module identity
If (GetSetting("MODULE-IDENTITY", mibIdentity, mibDummy)) Then
    Print #1, mibIdentity + TabChar + "MODULE-IDENTITY"
End If

' if the last-updated row is void, insert the current date
If (GetSetting("LAST-UPDATED", mibStr, mibDummy)) Then
    If mibStr = "" Then
        Dim dateStr As String
        Dim dashPos As Integer
        Dim segStr As String
        
        dateStr = Date
        dashPos = InStr(dateStr, "-")
        If dashPos <> 0 Then
            mibStr = """" + Left(dateStr, dashPos - 1) + Mid(dateStr, dashPos + 1, 2) + Mid(dateStr, dashPos + 4, 2)
        End If
        ' write date in ASN.1 format
        Dim timeStr As String
        timeStr = Mid(dateStr, 12, 5) 'Time
        dashPos = InStr(timeStr, ":")
        If dashPos <> 0 Then
            mibStr = mibStr + Left(timeStr, dashPos - 1) + Mid(timeStr, dashPos + 1, 2) + "Z""" ' only 4 digits for time of day
            mibStr = mibStr + " -- " + dateStr + " UTC"
        End If
    End If
    Print #1, TabChar + "LAST-UPDATED" + TabChar + mibStr
    ' Print #2, "Creation date: " + mibStr
End If

' read the organization
If (GetSetting("ORGANIZATION", mibStr, mibDummy)) Then
    Print #1, TabChar + "ORGANIZATION    " + mibStr
End If
' read the contact information (one line)
If (GetSetting("CONTACT-INFO", mibStr, mibDummy)) Then
    Print #1, TabChar + "CONTACT-INFO    " + mibStr
End If

' read the description (several lines)
Print #1, TabChar + "DESCRIPTION"
While (GetSetting("ORGDESCRIPTION", mibDescription, mibDummy))
   mibDescription = AdjustSMwording(mibDescription)
   call printTextWrap(TabChar + TabChar,  """" + mibDescription + """", 72)
Wend

Do 'loop for revisions
Dim mibComment As String
    If Not (GetSetting("REVISION", mibStr, mibComment)) Then
Exit Do
    End If
' if the revision date is void, insert the current date
    If mibStr = "" Then
        dateStr = utc 'Date
        dashPos = InStr(dateStr, "-")
        If dashPos <> 0 Then
            mibStr = """" + Left(dateStr, dashPos - 1) + Mid(dateStr, dashPos + 1, 2) + Mid(dateStr, dashPos + 4, 2)
        End If
        ' write date in ASN.1 format
        timeStr = Mid(dateStr, 12, 5) 'Time
        dashPos = InStr(timeStr, ":")
        If dashPos <> 0 Then
            mibStr = mibStr + Left(timeStr, dashPos - 1) + Mid(timeStr, dashPos + 1, 2) + "Z""" ' only 4 digits for time of day
            mibStr = mibStr + " -- " + dateStr + " UTC"
        End If
    Else
        mibStr = """" + mibStr + """ -- " + mibComment
    End If
    Print #1, TabChar + "REVISION" + TabChar + TabChar + mibStr

' get the description of the revision
    
    If (GetSetting("REVDESCRIPTION", mibDescription, mibDummy)) Then
        mibDescription = AdjustSMwording(mibDescription)
        Print #1, TabChar + "DESCRIPTION"
        call printTextWrap(TabChar + TabChar, """" + mibDescription + """", 72)
    End If
Loop

' read the inheritance
If GetSetting("MIB-INFO", mibStr, mibDummy) Then
    Print #1, TabChar + TabChar + mibStr
End If

'read the root object
If GetSetting("ROOT OBJECT", mibRoot, mibDummy) Then
    MibStackLevel = 1
    MibStackObject(MibStackLevel) = mibRoot
    MibStackCnt(MibStackLevel) = 0 ' initialize for first object
    End If

Dim objIdentifier As String
Dim mibIdentifier As String
Do
    If Not GetSetting("OBJECT IDENTIFIER", objIdentifier, mibIdentifier) Then
        Exit Do
    Else
        Print #1, objIdentifier + TabChar + "OBJECT IDENTIFIER ::= { " + mibIdentifier + " }"
    End If
Loop


Call EvaluateTypes
Call EvaluateEvents(mibRoot)
Call EvaluateObjects(mibRoot)
Call EvaluateConformance(mibRoot)

Print #1, "END"
Close #1
' Close #2
MsgBox ("MIB " + mibFileName + " ready")

End Sub 'MakeMIB

Private Sub EvaluateTypes()

Dim mibStr As String
Dim txtRow As Integer

' here we exit with MibStackObject = {module, ptpObject,....}, MibStackLevel = 2

Print #1, ""
Print #1, "--====================================================================="
Print #1, "-- Textual Conventions"
Print #1, "--====================================================================="
Print #1, ""

Call TxtHeaderSearch(SheetTextual) ' get the headings

txtRow = 3
While (SheetTextual.Cells(txtRow, TxtColControl) <> "END")
        mibStr = ""
        Print #1, mibStr
        mibStr = SheetTextual.Cells(txtRow, TxtColName) + " ::= TEXTUAL-CONVENTION"
        Print #1, mibStr
        'hint
        mibStr = SheetTextual.Cells(txtRow, TxtColHint)
        If mibStr <> "" Then
            mibStr = TabChar + "DISPLAY-HINT " + """" + mibStr + """"
            Print #1, mibStr
        End If
        'status
        mibStr = TabChar + "STATUS       " + "current"
        Print #1, mibStr
        'description
        mibStr = TabChar + "DESCRIPTION"
        Print #1, mibStr
        call printTextWrap(TabChar + TabChar, """" + CStr(SheetTextual.Cells(txtRow, TxtColDescription)) + """", 72)
        'reference
        mibStr = TabChar + "REFERENCE    " + """" + CStr(SheetTextual.Cells(txtRow, TxtColReference)) + """"
        'brake the line if too long
        if (len(mibStr) < 73 ) then
            Print #1, mibStr
        else
            mibStr = TabChar + "REFERENCE " + """" + CStr(SheetTextual.Cells(txtRow, TxtColReference)) + """"
            if (len(mibStr) < 73 ) then
                Print #1, mibStr
            else
                Print #1, TabChar + "REFERENCE"
                mibStr = """" + CStr(SheetTextual.Cells(txtRow, TxtColReference)) + """"
                mibStr = space(72 - len(mibStr)) + mibStr
                Print #1, mibStr
            end if
        end if
'         Print #1, mibStr
        'syntax
        mibStr = TabChar + "SYNTAX       " + CStr(SheetTextual.Cells(txtRow, TxtColSyntax)) + ""
        Print #1, mibStr
        'units
        mibStr = SheetTextual.Cells(txtRow, TxtColUnits)
        
        If mibStr <> "" Then
            mibStr = TabChar + "UNITS   " + """" + mibStr + """"
            Print #1, mibStr
        End If

        Print #1, ""
        txtRow = txtRow + 1
        
Wend
    
End Sub

Private Sub EvaluateObjects(objectRoot)

    Dim objectNr As Integer
    Dim objOID As String
    Dim objName As String
    Dim objType As String
    Dim objTree As String
    Dim objSyntax As String
    Dim objAccess As String
    Dim objStatus As String
    Dim objDescription As String
    Dim objReference As String

    Dim objRow As Integer   'row in the obbject sheet
    Dim indRow As Integer ' for the index
    Dim indStr As String 'for the index
        
    Dim mibStr As String
    Dim commentStr As String
    Dim parpos As Integer
    Dim parposSpace As Integer
    
    Print #1, ""
    Print #1, "--====================================================================="
    Print #1, "-- Data Sets"
    Print #1, "--====================================================================="
    Print #1, ""
        
Call ObjHeaderSearch(SheetObjects) ' order the headings
objRow = 3
objectNr = 0
ObjectLevel = 2 ' ptp level + 1
MibStackCnt(ObjectLevel) = 0 ' no objects yet

While (SheetObjects.Cells(objRow, ObjColControl) <> "END")
' Name is a mandatory field: but if void, there must be a reserved field this marks the end of the list.
    
    ' ================= Check entries ========================================================================

    If (SheetObjects.Cells(objRow, ObjColControl) <> "R") Then 'not a reserved field
    
        objTree = Trim(SheetObjects.Cells(objRow, ObjColTree))
        If (objTree = "}") Then
             ObjectLevel = ObjectLevel - 1
        ElseIf (objTree = "}}") Then
             ObjectLevel = ObjectLevel - 2
        ElseIf (objTree = "}}}") Then
             ObjectLevel = ObjectLevel - 3
        ElseIf (objTree = "}}}}") Then
             ObjectLevel = ObjectLevel - 4
        ElseIf (objTree = "}}}}}") Then
             ObjectLevel = ObjectLevel - 5
        ElseIf (objTree = "}}}}}}") Then
             ObjectLevel = ObjectLevel - 6
        Else
        ' this is not an entry
    
            objectNr = objectNr + 1
        
            objName = Trim(SheetObjects.Cells(objRow, ObjColName))

            objName = adjustSMwording(objName)
            If (Len(objName) > 64) Or (Len(objName) = 0) Then
                MsgBox ("Size error in object " + objOID + " " + objName)
                objName = Left(objName, 64)
            End If
            
            MibStackObject(ObjectLevel) = objName ' new level
            MibStackCnt(ObjectLevel) = MibStackCnt(ObjectLevel) + 1
            
            objOID = CStr(MibStackCnt(3))
            Dim j As Integer
            For j = 4 To (ObjectLevel)
               objOID = objOID + "." + CStr(MibStackCnt(j))
            Next j
            If (objTree <> "X") Then
                SheetObjects.Cells(objRow, ObjColOID) = "." + objOID
            End If
            'only place where one writes into the sheet.
           
            If (objTree = "B") Then ' This is a branch
                Print #1, ""

                commentStr = Trim(SheetObjects.Cells(objRow, ObjColDescription))
                If (commentStr <> "") Then
                    call printTextWrap("-- ", commentStr, 72)
                End If

                mibStr = objName + " OBJECT IDENTIFIER ::= { " + MibStackObject(ObjectLevel - 1) + " " + CStr(MibStackCnt(ObjectLevel)) + " }"
                'brake the line if too long
                if (len(mibStr) < 73 ) then
                    Print #1, mibStr
                else
                    Print #1, objName
                    mibStr = "OBJECT IDENTIFIER ::= { " + MibStackObject(ObjectLevel - 1) + " " + CStr(MibStackCnt(ObjectLevel)) + " }"
                    mibStr = space(72 - len(mibStr)) + mibStr
                    Print #1, mibStr
                end if
                ObjectLevel = ObjectLevel + 1 ' go down one level
                MibStackCnt(ObjectLevel) = 0 ' first object of level: no object yet
            'let browse the branch, only the index identation changes
                
             ElseIf (objTree = "X") Then  'table or entry
                mibStr = objName + " OBJECT-IDENTITY"
                Print #1, ""
                if (len(mibStr) < 73 ) then
                    Print #1, mibStr
                else
                    Print #1, objName
                    mibStr = "OBJECT-IDENTITY"
                    mibStr = space(72 - len(mibStr)) + mibStr
                    Print #1, mibStr
                end if

                'status
                objStatus = Trim(SheetObjects.Cells(objRow, ObjColStatus))
                If (objStatus = "") Then
                    objStatus = "current"
                End If
                Print #1, TabChar + "STATUS       " + objStatus ' always print STATUS
                ' description
                objDescription = Trim(SheetObjects.Cells(objRow, ObjColDescription))
                objDescription = adjustSMwording(objDescription)
                If (objDescription <> "") Then
                    Print #1, TabChar + "DESCRIPTION"
                    call printTextWrap(TabChar + TabChar,"""" + objDescription + """", 72) 
                End If

                ' reference
                objReference = Trim(SheetObjects.Cells(objRow, ObjColReference))
                If (objReference <> "") Then
'                     Print #1, TabChar + "REFERENCE    " + """" + objReference + """"
                    mibStr = TabChar + "REFERENCE    " + """" + objReference + """"
                    'brake the line if too long
                    if (len(mibStr) < 73 ) then
                        Print #1, mibStr
                    else
                        mibStr = TabChar + "REFERENCE " + """" + objReference + """"
                        if (len(mibStr) < 73 ) then
                            Print #1, mibStr
                        else
                            Print #1, TabChar + "REFERENCE"
                            mibStr = """" + objReference + """"
                            mibStr = space(72 - len(mibStr)) + mibStr
                            Print #1, mibStr
                        end if
                    end if
                End If

                mibStr = TabChar + "::= { " + MibStackObject(ObjectLevel - 1) + " " + CStr(SheetObjects.Cells(objRow, ObjColOID)) + " }"

                Print #1, mibStr

             ElseIf ((objTree = "T") Or (objTree = "E")) Then  'table or entry
                    
                mibStr = ""
                Print #1, mibStr
                mibStr = objName + " OBJECT-TYPE"
                Print #1, mibStr
            ' syntax
                objSyntax = Trim(SheetObjects.Cells(objRow, ObjColSyntax))
                objSyntax = adjustSMwording(objSyntax)

                If (objSyntax <> "") Then
                    mibStr = TabChar + "SYNTAX       " + objSyntax
                    'brake the line if too long
                    if (len(mibStr) < 73 ) then
                        Print #1, mibStr
                    ElseIf (len(TabChar + "SYNTAX " + objSyntax) < 73) then
                        mibStr = objSyntax
                        mibStr = space(72 - len(mibStr) - len(TabChar + "SYNTAX")) + mibStr
                        Print #1, TabChar + "SYNTAX" + mibStr
                    Else
                        Print #1, TabChar + "SYNTAX"
                        mibStr = objSyntax
                        mibStr = space(72 - len(mibStr)) + mibStr
                        Print #1, mibStr
                    end if
                End If
            'access
                objAccess = Trim(SheetObjects.Cells(objRow, ObjColAccess))
                If objAccess = "R/W" Then
                    mibStr = "read-write"
                ElseIf objAccess = "R/C" Then
                    mibStr = "read-create"
                ElseIf objAccess = "R" Then
                    mibStr = "read-only"
                Else
                    mibStr = "not-accessible"
                End If
                mibStr = TabChar + "MAX-ACCESS   " + mibStr
                Print #1, mibStr
            'status
                objStatus = Trim(SheetObjects.Cells(objRow, ObjColStatus))
                If (objStatus = "") Then
                    objStatus = "current"
                End If
                Print #1, TabChar + "STATUS       " + objStatus ' always print STATUS
            ' description
                objDescription = Trim(SheetObjects.Cells(objRow, ObjColDescription))
                objDescription = adjustSMwording(objDescription)
                If (objDescription <> "") Then
                    Print #1, TabChar + "DESCRIPTION"
                    call printTextWrap(TabChar + TabChar,"""" + objDescription + """", 72) 
                End If
            ' reference
                objReference = Trim(SheetObjects.Cells(objRow, ObjColReference))
                If (objReference <> "") Then
                    Print #1, TabChar + "REFERENCE    " + """" + objReference + """"
                End If
            ' get the indices after entry if applicable
                If objTree = "E" Then 'this is an entry
                    Dim indTree As String
                    indRow = objRow + 1 ' scan next index row
                    indTree = Trim(SheetObjects.Cells(indRow, ObjColTree))
                    
                    If indTree = "I" or indTree = "II" Then ' get the first index
                       Print #1, TabChar + "INDEX        " + "{"
                       Do
                            indStr = Trim(SheetObjects.Cells(indRow, ObjColName)) 'name of the index
                            indStr = adjustSMwording(indStr)
                            If indStr = "" Then
                                MsgBox ("Error in name at line " & indRow)
                            End If
                            indRow = indRow + 1
                            If Trim(SheetObjects.Cells(indRow, ObjColTree)) <> "R" Then 'skip reserved lines
                                indTree = Trim(SheetObjects.Cells(indRow, ObjColTree)) ' lookahead: is the next one also an index?
                                If indTree = "I" or indTree = "II" Then ' all indexes at the beginning of the list
                                   Print #1, TabChar + TabChar + indStr + "," 'it is an index
                                Else
                                  Exit Do 'the indices msut be continuous, therefore exit if the next is not an index
                                End If
                            End If
                       Loop
                    ' look for the indices
                    Print #1, TabChar + TabChar + indStr + " }" 'last item
                    End If
                End If
                    
                mibStr = TabChar + "::= { " + MibStackObject(ObjectLevel - 1) + " " + CStr(MibStackCnt(ObjectLevel)) + " }"
                Print #1, mibStr
                Print #1, "-- ." + objOID
               
                
                ObjectLevel = ObjectLevel + 1
                MibStackObject(ObjectLevel) = objName ' new object
                MibStackCnt(ObjectLevel) = 0 ' new object
                
                If objTree = "E" Then ' this is an entry
                    ' first declare the type
                    Print #1, vbCrLf
                    indRow = objRow + 1 'scan the index rows
                    indTree = Trim(SheetObjects.Cells(indRow, ObjColTree))
                    If indTree = "}" Then ' table is closed by a "}"
                        MsgBox "Void Table at " & objName
                    Else ' get the indices
                       Dim subRow As Integer
                       subRow = indRow
                       Print #1, objSyntax + TabChar + "::= SEQUENCE {"
                       Do
                         If Trim(SheetObjects.Cells(subRow, ObjColControl)) <> "R" Then ' skip the reserved fields
                             indTree = Trim(SheetObjects.Cells(subRow, ObjColTree))
                             If indTree <> "II" then
                                 indStr = Trim(SheetObjects.Cells(subRow, ObjColName))
                                 indStr = adjustSMwording(indStr)
	                             If indStr = "" Then
	                                MsgBox ("Error in table")
	                             End If
	                             indStr = TabChar + TabChar + indStr
	                             objType = Trim(SheetObjects.Cells(subRow, ObjColSyntax))
	                             parpos = InStr(objType, "(") '0 if not found
	                             parposSpace = InStr(objType, " ") '0 if not found
	                             'remove the parenthesis and its contents if it exists
	                             If parpos <> 0 Then
	                                 objType = Left(objType, parpos - 1)
	                             End If
	                             'if a space begins before parenthesis, take the position -1 of the space
	                             If (parposSpace <> 0) and (parposSpace < parpos) Then
	                                 objType = Left(objType, parposSpace - 1)
	                             End If
	                             'brake the line if too long
	                             if (len(indStr) + len (objType) + 1 < 72 ) then
	                                 indStr = indStr + " " + space(47-len(indStr)) + objType
	                             else
	                                 Print #1, indStr
	                                 indStr = space(45) + objType
	                             end if
	                             'lookahead for the next field
	                             While Trim(SheetObjects.Cells(subRow + 1, ObjColControl)) = "R" ' skip the reserved fields
	                                subRow = subRow + 1 'there is always an END.
	                             Wend
	                             
	                            indTree = Trim(SheetObjects.Cells(subRow + 1, ObjColTree))
	                            If (indTree = "}") Or (indTree = "}}") Or (indTree = "}}}") Or (indTree = "}}}}") Or (indTree = "}}}}}") Then
	                                Print #1, indStr 'the list is closed
	                                Print #1, TabChar + TabChar + "}"
	                                Exit Do
	                            End If
	                            indStr = indStr + "," 'there is a next, print a ","
	                            parpos = InStr(indStr, "SEQUENCE")
	                            If parpos <> 0 Then
	                                MsgBox "SEQUENCE OF SEQUENCE illegal in ASN.1, but understood in some SNMP"
	                            End If
	                            Print #1, indStr
                            End If ' indTree <> "II"
                         End If
                         subRow = subRow + 1
                       Loop
                    End If
             End If
                
         ElseIf ((objTree = "II")) then
             ' Do nothing
         Else ' it is an object
            
            mibStr = ""
            Print #1, mibStr
            mibStr = objName + " OBJECT-TYPE"
            'brake the line if too long
            if (len(mibStr) < 73 ) then
                Print #1, mibStr
            else
                Print #1, objName
                mibStr = "OBJECT-TYPE"
                mibStr = space(72 - len(mibStr)) + mibStr
                Print #1, mibStr
            end if
            ' Print #2, mibStr
        ' syntax
            objSyntax = Trim(SheetObjects.Cells(objRow, ObjColSyntax))
            objSyntax = adjustSMwording(objSyntax)
            If (objSyntax <> "") Then
                Print #1, TabChar + "SYNTAX       " + objSyntax
            End If
        'units
            mibStr = Trim(SheetObjects.Cells(objRow, ObjColUnits))
            If mibStr <> "" Then
                mibStr = TabChar + "UNITS        " + """" + mibStr + """"
                Print #1, mibStr
            End If
        'access
            objAccess = Trim(SheetObjects.Cells(objRow, ObjColAccess))
            If objAccess = "R/W" Then
                mibStr = "read-write"
            ElseIf objAccess = "R/C" Then
                mibStr = "read-create"
            ElseIf objAccess = "R" Then
                mibStr = "read-only"
            Else
                mibStr = "not-accessible"
            End If   'XXXXXXXXXXXXXXXXXXXXX
            mibStr = TabChar + "MAX-ACCESS   " + mibStr
            Print #1, mibStr
        'status
            objStatus = Trim(SheetObjects.Cells(objRow, ObjColStatus))
            If (objStatus = "") Then
                objStatus = "current"
            End If
            Print #1, TabChar + "STATUS       " + objStatus ' always print STATUS
        ' description
            objDescription = Trim(SheetObjects.Cells(objRow, ObjColDescription))
            objDescription = adjustSMwording(objDescription)
            If (objDescription <> "") Then
                Print #1, TabChar + "DESCRIPTION"
                call printTextWrap(TabChar + TabChar, """" + objDescription + """", 72)
                ' Print #2, mibStr
            End If
        ' reference
            objReference = Trim(SheetObjects.Cells(objRow, ObjColReference))
            If (objReference <> "") Then
                mibStr = TabChar + "REFERENCE    " + """" + objReference + """"
                'brake the line if too long
                if (len(mibStr) < 73 ) then
                    Print #1, mibStr
                else
                    mibStr = TabChar + "REFERENCE    " + """" + objReference + """"
                    if (len(mibStr) < 73 ) then
                        Print #1, mibStr
                    else
                        Print #1, TabChar + "REFERENCE"
                        mibStr = """" + objReference + """"
                        mibStr = space(72 - len(mibStr)) + mibStr
                        Print #1, mibStr
                    end if
                end if
            End If
            
        'default value
            
            mibStr = Trim(SheetObjects.Cells(objRow, ObjColDefVal))
            'convert from 0x hex format to 'H
            parpos = InStr(mibStr, "0x") '0 if not found
            If parpos = 1 Then
                mibStr = Right(mibStr, Len(mibStr) - 2)
                mibStr = "'" + mibStr + "'H"
            Else
                'add leading ' if not present in hex format (Excel bug)
                parpos = InStr(mibStr, "'H") '0 if not found
                If parpos <> 0 Then 'found, add the apostrophe
                    parpos = InStr(mibStr, "'")
                    If parpos <> 1 Then 'there was no start apostrophe, add it
                        mibStr = "'" + mibStr
                    End If
                End If
            End If
    
            If mibStr <> "" Then
                mibStr = TabChar + "DEFVAL           " + "{ " + mibStr + " }"
                Print #1, mibStr
            End If
            
            mibStr = TabChar + "::= { " + MibStackObject(ObjectLevel - 1) + " " + CStr(MibStackCnt(ObjectLevel)) + " }"
            
            Print #1, mibStr
            
            Print #1, "-- ." + objOID
        End If
    End If ' end object
     
    End If ' this was not a reserved field
    objRow = objRow + 1
    
Wend
End Sub 'evaluate objects

Private Sub EvaluateEvents(eventRoot As String)

    Print #1, ""
    Print #1, "--====================================================================="
    Print #1, "-- Events"
    Print #1, "--====================================================================="
    Print #1, ""

' Global ObjectLevel

Dim evtRow As Integer 'processed row on that sheet

Call EvtHeaderSearch(SheetEvents) ' order the headings, same for "objects, textual and events"

evtRow = 3
ObjectLevel = 2
Dim evtName As String
Dim mibStr As String
Dim commentStr As String

Do
    evtName = Trim(SheetEvents.Cells(evtRow, EvtColName))
    If evtName = "" Then
Exit Do
    End If

    mibStr = Trim(SheetEvents.Cells(evtRow, EvtColTree))
    If (mibStr = "B") Then
        'This is a branch
        commentStr = Trim(SheetEvents.Cells(evtRow, EvtColDescription))
        If (commentStr <> "") Then
            call printTextWrap("-- ", commentStr, 72)
        End If
        mibStr = evtName + " OBJECT IDENTIFIER ::= { " + MibStackObject(ObjectLevel - 1) + " " + CStr(0) + " }" 'MibStackCnt(ObjectLevel)
        Print #1, mibStr
        MibStackCnt(ObjectLevel - 1) = MibStackCnt(ObjectLevel - 1) + 1 ' new object
        MibStackObject(ObjectLevel) = evtName ' new object
        MibStackCnt(ObjectLevel) = 1 ' new object
        ObjectLevel = ObjectLevel + 1
    Else
        'not a branch
    
        mibStr = ""
        Print #1, mibStr
        mibStr = evtName + " NOTIFICATION-TYPE"
        Print #1, mibStr
    
        mibStr = Trim(SheetEvents.Cells(evtRow, EvtColTrigger))
        If (mibStr <> "") Then
            Print #1, TabChar + "OBJECTS      " + "{ " + mibStr + " }"
        End If
        
        mibStr = Trim(SheetEvents.Cells(evtRow, EvtColStatus))
        If (mibStr = "") Then
            mibStr = "current"
        End If
        Print #1, TabChar + "STATUS       " + mibStr 'always put status

        mibStr = Trim(SheetEvents.Cells(evtRow, EvtColDescription))
        EvtColDescription = adjustSMwording(EvtColDescription)
        If (mibStr <> "") Then
            Print #1, TabChar + "DESCRIPTION"
            call printTextWrap(TabChar + TabChar, """" + mibStr + """", 72)
        End If
        
        mibStr = TabChar + "::= { " + MibStackObject(ObjectLevel - 1) + " " + CStr(MibStackCnt(ObjectLevel - 1)) + " }"
        MibStackCnt(ObjectLevel - 1) = MibStackCnt(ObjectLevel - 1) + 1 ' increment object number for next time
        Print #1, mibStr
    End If
    
    ' don't add notification objects
    exit sub
    evtRow = evtRow + 1
Loop
End Sub

Private Sub EvaluateConformance(conformanceRoot As String)

Dim grpGroupObject As String
Dim grpGroupName As String
Dim grpIndex As Integer
Dim grpRow As Integer 'row in the Groups sheet

Dim objRow As Integer 'row in the Objects sheet
Dim objGroup As String
Dim objName As String

Dim evtRow As Integer 'row in the Events sheet
Dim evtName As String   ' for the notification group
Dim evtGroup As String  ' group name of that event

Dim mibStr As String
Dim grpStatus As String

Dim mandatoryComplianceEntries As Integer


    Print #1, ""
    Print #1, "--====================================================================="
    Print #1, "-- Conformance Information"
    Print #1, "--====================================================================="
    Print #1, ""
    
Call GrpHeaderSearch(SheetGroups)

grpRow = 3
ObjectLevel = 2
MibStackCnt(ObjectLevel) = MibStackCnt(ObjectLevel) + 1 'increase the index on my stack level

grpIndex = 0
grpGroupObject = Trim(SheetGroups.Cells(grpRow, GrpColName))
mibStr = grpGroupObject + " OBJECT IDENTIFIER ::= { " + MibStackObject(ObjectLevel - 1) + " " + CStr(2) + " }" 'MibStackCnt(ObjectLevel)
Print #1, mibStr

grpRow = grpRow + 1 'get first supergroup

MibStackObject(ObjectLevel) = grpGroupObject 'create new branch for a group
ObjectLevel = ObjectLevel + 1 'increase the level
MibStackCnt(ObjectLevel) = 1   'first object here

grpGroupObject = Trim(SheetGroups.Cells(grpRow, GrpColName)) 'get first group
mibStr = grpGroupObject + " OBJECT IDENTIFIER ::= { " + MibStackObject(ObjectLevel - 1) + " " + CStr(MibStackCnt(ObjectLevel)) + " }"
Print #1, mibStr

MibStackCnt(ObjectLevel - 1) = MibStackCnt(ObjectLevel - 1) + 1 'increase the index on above stack level

MibStackObject(ObjectLevel) = grpGroupObject 'create new branch
MibStackCnt(ObjectLevel) = 1   'first object here
ObjectLevel = ObjectLevel + 1 ' increase the level

' get first group, skip reserved
Do
    grpGroupName = Trim(SheetGroups.Cells(grpRow, GrpColName)) 'look for the first object in the list
    grpRow = grpRow + 1
    If (grpGroupName <> "") And (Trim(SheetGroups.Cells(grpRow, GrpColControl)) <> "R") Then
        Exit Do
    End If
Loop
   

If (Trim(SheetGroups.Cells(grpRow, GrpColControl)) <> "END") Then 'at least one entry
    Do ' loop to get all groups
        grpGroupName = Trim(SheetGroups.Cells(grpRow, GrpColName))
        grpGroupName = adjustSMwording(grpGroupName)
        If grpGroupName = "" Then
    Exit Do
        End If 'name exists
        If (Trim(SheetGroups.Cells(grpRow, GrpColControl)) <> "R") Then
            grpIndex = grpIndex + 1
            GrpGroup(grpIndex) = grpGroupName ' new group detected, add it to group table
            If SheetGroups.Cells(grpRow, GrpColOption) = "O" Then ' object group
            
                mibStr = ""
                objRow = 3 ' scan the object sheet
                
                ' look for first object
                Do
                    If (SheetObjects.Cells(objRow, ObjColControl) = "END") Then
                Exit Do
                    End If
                    If (SheetObjects.Cells(objRow, ObjColControl) <> "R") Then
                        objGroup = Trim(SheetObjects.Cells(objRow, ObjColGroup))
                        objGroup = adjustSMwording(objGroup)
                        If (objGroup = grpGroupName) Then
                            objName = Trim(SheetObjects.Cells(objRow, ObjColName))
                            objName = adjustSMwording(objName)
                            objRow = objRow + 1
                            mibStr = TabChar + TabChar + objName   ' 1st object of that group found
                Exit Do
                        End If
                    End If ' not reserved
                    objRow = objRow + 1
                Loop
                
                If mibStr <> "" Then 'a first object with the correct group was found, get the next
                    Print #1, grpGroupName + "  OBJECT-GROUP"
                    Print #1,  TabChar + "OBJECTS {"
                
                    Do
                        If (SheetObjects.Cells(objRow, ObjColControl) = "END") Then
                    Exit Do ' this marks the end of the list (there are no entries on that line)
                        End If
                        If (SheetObjects.Cells(objRow, ObjColControl) <> "R") Then
                            objGroup = Trim(SheetObjects.Cells(objRow, ObjColGroup))
                            objGroup = adjustSMwording(objGroup)
                            If (objGroup = grpGroupName) Then 'next one exists
                                objName = Trim(SheetObjects.Cells(objRow, ObjColName)) ' get next name
                                objName = adjustSMwording(objName)
                                Print #1, mibStr + "," 'preceeding object
                                mibStr = TabChar + TabChar + objName 'next object
                            End If
                        End If 'not reserved
                        objRow = objRow + 1 ' get next object
                    Loop
            
                     Print #1, mibStr ' last object
                     Print #1, TabChar + TabChar + "}"
                     grpStatus = Trim(SheetGroups.Cells(grpRow, GrpColStatus))
                     grpStatus = adjustSMwording(grpStatus)
                     If (grpStatus = "") Then
                         grpStatus = "current"
                     End If
                    
                     Print #1, TabChar + "STATUS       " + grpStatus
                     Print #1, TabChar + "DESCRIPTION"
                     call printTextWrap(TabChar + TabChar, """" + CStr(adjustSMwording(SheetGroups.Cells(grpRow, GrpColDescription))) + """", 72)
                     Print #1, TabChar + "::= { " + grpGroupObject + " " + CStr(grpIndex) + " }"
                     Print #1, vbCrLf
                 End If ' one object found
    
            ElseIf SheetGroups.Cells(grpRow, GrpColOption) = "N" Then
            
                mibStr = ""
                evtRow = 3 ' start on top of event list
                
                ' get first object of that notification group
                Do
                    evtName = Trim(SheetEvents.Cells(evtRow, EvtColName))
                    evtName = adjustSMwording(evtName)
                    If (evtName = "") Or (Trim(SheetEvents.Cells(evtRow, EvtColControl)) = "END") Then
                        Exit Do
                    End If
                    evtGroup = Trim(SheetEvents.Cells(evtRow, EvtColGroup))
                    evtGroup = adjustSMwording(evtGroup)
                    If (evtGroup = grpGroupName) Then
                        mibStr = TabChar + evtName   ' first object found
                        evtRow = evtRow + 1
                Exit Do
                    End If
                    evtRow = evtRow + 1
                Loop
                
                If mibStr <> "" Then 'an object with the correct group was found
                Print #1, grpGroupName + "  NOTIFICATION-GROUP"
                Print #1, "  NOTIFICATIONS {"
                
                    Do
                        evtName = Trim(SheetEvents.Cells(evtRow, EvtColName)) ' get next object name
                        evtName = adjustSMwording(evtName)
                        If (evtName = "") Or Trim(SheetObjects.Cells(evtRow, EvtColName)) = "END" Then
                    Exit Do ' Name is a mandatory field: if void, this marks the end of the list.
                        End If
                        evtGroup = Trim(SheetEvents.Cells(evtRow, EvtColGroup))
                        evtGroup = adjustSMwording(evtGroup)
                        If (evtGroup = grpGroupName) Then 'next one exists
                            Print #1, mibStr + "," 'preceeding object
                            mibStr = TabChar + evtName
                        End If
                        evtRow = evtRow + 1 ' get next object
                    Loop
            
                Print #1, mibStr ' last object
                Print #1, TabChar + "}"
                grpStatus = Trim(SheetGroups.Cells(grpRow, GrpColStatus))
                grpStatus = adjustSMwording(grpStatus)

                If (grpStatus = "") Then
                    grpStatus = "current"
                End If
                Print #1, TabChar + "STATUS " + grpStatus
                Print #1, TabChar + "DESCRIPTION"
                call printTextWrap(TabChar + TabChar, """" + CStr(adjustSMwording(SheetGroups.Cells(grpRow, GrpColDescription))) + """", 72)
                Print #1, TabChar + "::= { " + grpGroupObject + " " + CStr(grpIndex) + " }"
                Print #1, vbCrLf
            
                End If
            End If ' nor Object nor Notification
        End If ' not a reserved field
        grpRow = grpRow + 1
        
    Loop ' all groups, exit on no name
    
End If 'at least one entry

GrpEntries = grpIndex

    Print #1, ""
    Print #1, "--====================================================================="
    Print #1, "-- MIB module Compliance statements"
    Print #1, "--====================================================================="
    Print #1, ""
    
' '=============================!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    Print #1, "ieee1588Compliances OBJECT IDENTIFIER ::= { ieee1588Conformance 2 }"
' '=============================!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
'     
    Print #1, "ieee1588Compliance MODULE-COMPLIANCE"
    Print #1, TabChar +"STATUS current"
    Print #1, TabChar + "DESCRIPTION"
    call printTextWrap(TabChar + TabChar, """The compliance statement for " + mibDefinition + " groups""", 72)
'                         
    grpRow = 5  'start on line 5
    mibStr = ""
    Print #1, TabChar + "MODULE"


    ' Check if there are any current mandatory compliance entries
    Do
        If (Trim(SheetGroups.Cells(grpRow, GrpColControl)) = "END") Then
            ' No mandatory current compliance entries
            mandatoryComplianceEntries = 0
            Exit Do
        End If

        If SheetGroups.Cells(grpRow, GrpColStatus) = "current" and SheetGroups.Cells(grpRow, GrpColConformance) = "mandatory" Then 'skip others
            ' There is at least one mandatory current compliance entry
            mandatoryComplianceEntries = 1
            Exit Do
        End If
        grpRow = grpRow + 1
    Loop

    If (mandatoryComplianceEntries = 1) Then
        Print #1, TabChar + TabChar + "MANDATORY-GROUPS {"
        Do
            If (Trim(SheetGroups.Cells(grpRow, GrpColControl)) = "END") Then
                Exit Do
            End If

            If SheetGroups.Cells(grpRow, GrpColStatus) = "current" and SheetGroups.Cells(grpRow, GrpColConformance) = "mandatory" Then 'skip others
                grpGroupObject = Trim(SheetGroups.Cells(grpRow, GrpColName))
                grpGroupObject = AdjustSMwording(grpGroupObject)
                If mibStr <> "" Then
                    Print #1, mibStr + ","
                End If
                mibStr = TabChar + TabChar + TabChar + grpGroupObject
            End If
            grpRow = grpRow + 1
        Loop
        ' close
        Print #1, mibStr
        Print #1, TabChar + TabChar + "}"
        Print #1, ""
    End If

    grpRow = 5  'start on line 5

    ' Add optional current compliance entries
    Do
        If (Trim(SheetGroups.Cells(grpRow, GrpColControl)) = "END") Then
            Exit Do
        End If

        If SheetGroups.Cells(grpRow, GrpColStatus) = "current" and SheetGroups.Cells(grpRow, GrpColConformance) = "optional" Then 'skip others
            grpGroupObject = Trim(SheetGroups.Cells(grpRow, GrpColName))
            grpGroupObject = AdjustSMwording(grpGroupObject)

            Print #1, TabChar + TabChar + "GROUP " + grpGroupObject
            Print #1, TabChar + TabChar + "DESCRIPTION"
            Print #1, TabChar + TabChar + TabChar + """This group is optional."""
            Print #1, ""
        End If
        grpRow = grpRow + 1
    Loop


    Print #1, TabChar + TabChar + "::= { ieee1588Compliances 1 }"
    Print #1, ""

'=============================!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
' deprecated objects

    Print #1, "ieee1588ComplianceDeprecated MODULE-COMPLIANCE"
    Print #1, TabChar + "STATUS deprecated"
    Print #1, TabChar + "DESCRIPTION"
    call printTextWrap(TabChar + TabChar, """The compliance statement for deprecated " + mibDefinition + " groups""",  72)

    grpRow = 5  'start on line 5
    mibStr = ""
    Print #1, TabChar + "MODULE"


    ' Check if there are any deprecated mandatory compliance entries
    Do
        If (Trim(SheetGroups.Cells(grpRow, GrpColControl)) = "END") Then
            ' No mandatory deprecated compliance entries
            mandatoryComplianceEntries = 0
            Exit Do
        End If

        If SheetGroups.Cells(grpRow, GrpColStatus) = "deprecated" and SheetGroups.Cells(grpRow, GrpColConformance) = "mandatory" Then 'skip others
            ' There is at least one mandatory deprecated compliance entry
            mandatoryComplianceEntries = 1
            Exit Do
        End If
        grpRow = grpRow + 1
    Loop

    If (mandatoryComplianceEntries = 1) Then
        Print #1, TabChar + TabChar + "MANDATORY-GROUPS {"
        Do
            If (Trim(SheetGroups.Cells(grpRow, GrpColControl)) = "END") Then
                Exit Do
            End If

            If SheetGroups.Cells(grpRow, GrpColStatus) = "deprecated" and SheetGroups.Cells(grpRow, GrpColConformance) = "mandatory" Then 'skip others
                grpGroupObject = Trim(SheetGroups.Cells(grpRow, GrpColName))
                If mibStr <> "" Then
                    Print #1, mibStr + ","
                End If
                mibStr = TabChar + TabChar + TabChar + grpGroupObject
            End If
            grpRow = grpRow + 1
        Loop
        ' close
        Print #1, mibStr
        Print #1, TabChar + TabChar + "}"
        Print #1, ""
    End If

    grpRow = 5  'start on line 5

    ' Add optional deprecated compliance entries
    Do
        If (Trim(SheetGroups.Cells(grpRow, GrpColControl)) = "END") Then
            Exit Do
        End If

        If SheetGroups.Cells(grpRow, GrpColStatus) = "deprecated" and SheetGroups.Cells(grpRow, GrpColConformance) = "optional" Then 'skip others
            grpGroupObject = Trim(SheetGroups.Cells(grpRow, GrpColName))

            Print #1, TabChar + TabChar + "GROUP " + grpGroupObject
            Print #1, TabChar + TabChar + "DESCRIPTION"
            Print #1, TabChar + TabChar + TabChar + """This group is optional."""
            Print #1, ""
        End If
        grpRow = grpRow + 1
    Loop
    Print #1, TabChar + TabChar + "::= { ieee1588Compliances 2 }"
    Print #1, ""
'=============================!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
' obsolete objects

'     Print #1, TabChar + "ieee1588ComplianceObsolete MODULE-COMPLIANCE"
'     Print #1, TabChar + "STATUS obsolete"
'     Print #1, TabChar + "DESCRIPTION"
'     mibStr = TabChar + TabChar+"""The compliance statement for obsolete " + mibDefinition + " groups"""
'     Print #1, mibStr
' 
'     grpRow = 5  'start on line 5
'     mibStr = ""
'     Print #1, "MODULE"
'         Print #1, "MANDATORY-GROUPS {"
'     
'         Do
'             If (Trim(SheetGroups.Cells(grpRow, GrpColControl)) = "END") Then
'         Exit Do
'             End If
'             If SheetGroups.Cells(grpRow, GrpColStatus) = "obsolete" Then 'skip reserved
'                 grpGroupObject = Trim(SheetGroups.Cells(grpRow, GrpColName))
'                If mibStr <> "" Then
'                    Print #1, mibStr + ","
'                 End If
'                 mibStr = TabChar + TabChar + grpGroupObject
'             End If
'             grpRow = grpRow + 1
'         Loop
'         ' close
'         Print #1, mibStr
'         Print #1, TabChar + "}"
'         Print #1, "::= { ieee1588Compliances 3 }"
End Sub

Private Sub TxtHeaderSearch(Sheet As Excel.Worksheet)    ' same for all
    Dim column As Integer   ' counter of column
    Dim heading As String   ' contents of first row
    Dim missing As String
    
    'identify the input columns according to their header
   
    column = 1
    While (Sheet.Cells(1, column) <> "") ' the last column heading MUST be void
        heading = Sheet.Cells(1, column)
        If heading = "Control" Then
            TxtColControl = column
        ElseIf heading = "MIB Name" Then TxtColName = column
        ElseIf heading = "MIB Description" Then TxtColDescription = column
        ElseIf heading = "MIB Reference" Then TxtColReference = column
        ElseIf heading = "MIB Syntax" Then TxtColSyntax = column
        ElseIf heading = "MIB Units" Then TxtColUnits = column
        ElseIf heading = "MIB Hint" Then TxtColHint = column
      End If
        column = column + 1
    Wend
    ' debugging
    missing = ""
    If TxtColControl = 0 Then missing = missing & " Control,"
    If TxtColName = 0 Then missing = missing & " Name,"
    If TxtColUnits = 0 Then missing = missing & " Units,"
    If TxtColDescription = 0 Then missing = missing & " " & " Description,"
    If TxtColReference = 0 Then missing = missing & " " & " Reference"
    If TxtColSyntax = 0 Then missing = missing & " " & " Syntax,"
    If TxtColHint = 0 Then missing = missing & " " & " Hint,"
    
    If missing <> "" Then
        MsgBox ("missing columns in sheet Textual: " + missing)
    End If
    
End Sub

Private Sub ObjHeaderSearch(Sheet As Excel.Worksheet)    ' same for all
    Dim column As Integer   ' counter of column
    Dim heading As String   ' contents of first row
    Dim missing As String
    
    'identify the input columns according to their header
   
    column = 1
    While (Sheet.Cells(1, column) <> "") ' the last column heading MUST be void
        heading = Sheet.Cells(1, column)
        If heading = "Control" Then
            ObjColControl = column
        ElseIf heading = "MIB OID" Then ObjColOID = column
        ElseIf heading = "MIB Tree" Then ObjColTree = column
        ElseIf heading = "MIB Name" Then ObjColName = column
        ElseIf heading = "MIB Units" Then ObjColUnits = column
        ElseIf heading = "MIB DefVal" Then ObjColDefVal = column
        ElseIf heading = "MIB Access" Then ObjColAccess = column
        ElseIf heading = "MIB Status" Then ObjColStatus = column
        ElseIf heading = "MIB Description" Then ObjColDescription = column
        ElseIf heading = "MIB Reference" Then ObjColReference = column
        ElseIf heading = "MIB Syntax" Then ObjColSyntax = column
        ElseIf heading = "MIB Hint" Then ObjColHint = column
        ElseIf heading = "MIB Group" Then ObjColGroup = column
      End If
        column = column + 1
    Wend
    ' debugging
    missing = ""
    If ObjColControl = 0 Then missing = missing & " Control,"
    If ObjColOID = 0 Then missing = missing & " OID,"
    If ObjColName = 0 Then missing = missing & " Name,"
    If ObjColUnits = 0 Then missing = missing & " Units,"
    If ObjColDefVal = 0 Then missing = missing & " DefVal,"
    If ObjColAccess = 0 Then missing = missing & " Access,"
    If ObjColStatus = 0 Then missing = missing & " " & "Status,"
    If ObjColDescription = 0 Then missing = missing & " " & " Description,"
    If ObjColReference = 0 Then missing = missing & " " & " Reference"
    If ObjColSyntax = 0 Then missing = missing & " " & " Syntax,"
    If ObjColHint = 0 Then missing = missing & " " & " Hint,"
    If ObjColGroup = 0 Then missing = missing & " " & " Group,"
    
    If missing <> "" Then
        MsgBox ("missing columns in sheet Objects: " + missing)
    End If
    
End Sub

Private Sub EvtHeaderSearch(Sheet As Excel.Worksheet)    ' same for all
    Dim column As Integer   ' counter of column
    Dim heading As String   ' contents of first row
    Dim missing As String
    
    
    'identify the input columns according to their header
   
    column = 1
    While (Sheet.Cells(1, column) <> "") ' the last column heading MUST be void
        heading = Sheet.Cells(1, column)
        If heading = "Control" Then
            EvtColControl = column
        ElseIf heading = "MIB Tree" Then EvtColTree = column
        ElseIf heading = "MIB Name" Then EvtColName = column
        ElseIf heading = "MIB Status" Then EvtColStatus = column
        ElseIf heading = "MIB Description" Then EvtColDescription = column
        ElseIf heading = "MIB Trigger" Then EvtColTrigger = column
        ElseIf heading = "MIB Group" Then EvtColGroup = column
      End If
        column = column + 1
    Wend
    ' debugging
    missing = ""
    If EvtColControl = 0 Then missing = missing & " Control"
    If EvtColTree = 0 Then missing = missing & " MIB Tree"
    If EvtColName = 0 Then missing = missing & " MIB Name"
    If EvtColStatus = 0 Then missing = missing & " MIB Status"
    If EvtColDescription = 0 Then missing = missing & " MIB Description"
    If EvtColTrigger = 0 Then missing = missing & " MIB Trigger"
    If EvtColGroup = 0 Then missing = missing & " MIB Group"
    If missing <> "" Then
        MsgBox ("missing columns in sheet Events: " + missing)
    End If
    
End Sub

Private Sub GrpHeaderSearch(Sheet As Excel.Worksheet)    ' same for all
    Dim column As Integer   ' counter of column
    Dim heading As String   ' contents of first row
    Dim missing As String
    
    
    'identify the input columns according to their header
   
    column = 1
    While (Sheet.Cells(1, column) <> "") ' the last column heading MUST be void
        heading = Sheet.Cells(1, column)
        If heading = "Control" Then
            GrpColControl = column
        ElseIf heading = "MIB Name" Then GrpColName = column
        ElseIf heading = "MIB Status" Then GrpColStatus = column
        ElseIf heading = "MIB Description" Then GrpColDescription = column
        ElseIf heading = "MIB Option" Then GrpColOption = column
        ElseIf heading = "MIB Conformance" Then GrpColConformance = column
      End If
        column = column + 1
    Wend
    ' debugging
    missing = ""
    If GrpColControl = 0 Then missing = missing & " MIB Control"
    If GrpColName = 0 Then missing = missing & " MIB Name"
    If GrpColStatus = 0 Then missing = missing & " MIB Status"
    If GrpColDescription = 0 Then missing = missing & " MIB Description"
    If GrpColOption = 0 Then missing = missing & " MIB Option"
    If GrpColConformance = 0 Then missing = missing & " MIB Conformance"
    If missing <> "" Then
        MsgBox ("missing columns in sheet Groups: " + missing)
    End If
    
End Sub

Private Function GetSetting(Key As String, Entry1 As String, Par As String) As Boolean
'retrieves a string from the table
'if the same key is entered again, gets the next occurence in the list
'for this, a global variable is needed: SettingsCursor
'if there is no entry, returns a void string.

    Dim setRow As Integer
    Dim keyText As String
    setRow = SettingsCursor

    Do
        keyText = SheetSettings.Cells(setRow, 1)
        keyText = Trim(keyText)
        If (keyText = Key) Then
            GetSetting = True
            Entry1 = SheetSettings.Cells(setRow, 2)
            SettingsCursor = setRow + 1 'store to continue at same place
            Par = SheetSettings.Cells(setRow, 3)
    Exit Do
        ElseIf (keyText = "END") Or (keyText = "") Then 'not found
            GetSetting = False
            SettingsCursor = 1
            Par = ""
    Exit Do
        Else
            setRow = setRow + 1
        End If
    Loop
End Function

Private Sub printTextWrap(prefix as String, textToPrint as String, textWidth as Integer)
    Dim strStart As Integer
    Dim strEnd As Integer

    strStart = 0
    Do
    	strStart = strEnd + 1 
		strEnd = findWrapPos(textToPrint, strStart, textWidth - len(prefix))

		Print #1, prefix + mid(textToPrint, strStart, strEnd - strStart)

	Loop while len(textToPrint) > strEnd
End Sub

Private Sub findWrapPos(strToWrap As String, startIndex As Integer, textWidth as Integer) as Integer
    Dim spacePosNew As Integer
    Dim spacePosOld As Integer
    Dim linePosNew As Integer

    spacePosOld = 0
    spacePosNew = startIndex
   
    Do
    	spacePosOld = spacePosNew

		' Search for a newline inside a cell
		linePosNew = InStr(spacePosNew, strToWrap, vbNewLine)
		spacePosNew = InStr(spacePosNew, strToWrap, " ")

		If linePosNew <> 0 and linePosNew < spacePosNew then
			spacePosNew = linePosNew
		End If

        if (spacePosNew) - startIndex > textWidth Then
			findWrapPos =  spacePosOld - 1
			Exit Sub
		End If
        
		' handle a newline inside a cell
		If linePosNew <> 0 and (linePosNew <= spacePosNew or spacePosNew = 0) Then
			findWrapPos = linePosNew
			Exit Sub
		End If

		' handle last word in text
		If spacePosNew = 0 Then
			spacePosNew = len(strToWrap)
			' last word does not fit into last line
			If (spacePosNew) - startIndex < textWidth Then
				findWrapPos = spacePosNew + 1
				Exit Sub
			else
				findWrapPos = spacePosOld - 1
				Exit Sub
			End If
		End If
		' If word is longer than textWidth
		If  spacePosOld = startIndex and spacePosNew - spacePosOld > textWidth Then
			findWrapPos = spacePosNew
			Exit Sub
		End If
		spacePosNew = spacePosNew+1

	Loop while (spacePosNew - 1) - startIndex <= textWidth
	findWrapPos = spacePosOld - 1
End Sub


Sub BuildArrayForSmReplacement()
	Dim oSheet As Object
	Dim oCursor As Object
	Dim oDoc As Object
	Dim oCellRange As Object
	Dim i As Integer

	' Get the active spreadsheet document
	oDoc = ThisComponent

	' Specify the sheet name you want to work with
	Dim sheetName As String
	sheetName = "MsReplacementList"
	
	' Get the sheet by name
	oSheet = oDoc.Sheets.getByName(sheetName)

	oCursor = oSheet.createCursor
	oCursor.gotoEndOfUsedArea(False)
' 	    MsgBox("last row " + str(oCursor.RangeAddress.EndRow))
	' Define the range from the first cell of the first column to the last cell with content
	oCellRange = oSheet.getCellRangeByPosition(0, 2, 0, oCursor.RangeAddress.EndRow)
	
	' Get the data from the range and store it in the array
	wordArraySm = oCellRange.getDataArray()

	If wordingVersion = "TT" then
		' Define the range from the first cell of the first column to the last cell with content
		oCellRange = oSheet.getCellRangeByPosition(1, 2, 1, oCursor.RangeAddress.EndRow)
	End If

	' Get the data from the range and store it in the array
	wordArrayTr = oCellRange.getDataArray()

End Sub


Private Sub AdjustSMwording(objName as String) as String
    
	Dim tmpPos As Integer
	Dim i as Integer

	tmpPos = Instr(objName, "<<")
	if tmpPos = 0 then
		adjustSMwording = objName
		Exit Sub
	End If

	tmpPos = Instr(objName, ">>")
	if tmpPos = 0 then
		MsgBox ("Found <<, but not >> in """ + objName + """")
		adjustSMwording = objName
		Exit Sub
	End If

	For i = LBound(wordArraySm) To UBound(wordArraySm)
	    objName = Replace(objName, "<<" + wordArraySm(i)(0) + ">>", wordArrayTr(i)(0))
	Next i

	adjustSMwording = objName

End Sub

